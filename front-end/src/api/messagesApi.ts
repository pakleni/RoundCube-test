import { Message } from '../types';
import { getBaseUrl } from './utils';

export const getMessages = async (): Promise<Message[]> => {
  const response = await fetch(getBaseUrl() + '/messages/', {
    method: 'GET'
  });

  const body = await response.json();

  if (response.ok) {
    return body as Message[];
  } else {
    throw body.message;
  }
};

export const newMessage = async (data: Message): Promise<void> => {
  const response = await fetch(getBaseUrl() + '/messages/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });

  const body = await response.json();

  if (!response.ok) {
    throw body.message;
  }
};
