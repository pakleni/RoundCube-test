import React from 'react';
import { Message } from '../types';

interface MessageComponentProps {
  data: Message;
}

const MessageComponent: React.FC<MessageComponentProps> = ({ data }) => {
  const { name, message } = data;
  return (
    <>
      <hr />
      <u>{name}</u>
      <p>{message}</p>
      <hr />
    </>
  );
};

export default MessageComponent;
