import React, { ReactElement } from 'react';
import { Route, Switch } from 'react-router-dom';
import Page404 from './pages/404';
import Home from './pages/Home';
import MessagePage from './pages/MessagePage';

const App: React.FC = (): ReactElement => {
  return (
    <>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/messages" component={MessagePage} />
        <Route component={Page404} />
      </Switch>
    </>
  );
};

export default App;
