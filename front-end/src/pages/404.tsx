import React, { ReactElement } from 'react';

const Page404: React.FC = (): ReactElement => <>404</>;

export default Page404;
