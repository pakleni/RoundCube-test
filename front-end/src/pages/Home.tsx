import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getMessages } from '../api/messagesApi';
import MessageComponent from '../components/MessageComponent';
import { Message } from '../types';

const App: React.FC = () => {
  const [loading, setLoading] = useState(true);
  const [messages, setMessages] = useState<Message[]>([]);

  const onLoad = async () => {
    setLoading(true);
    setMessages(await getMessages());
    setLoading(false);
  };

  useEffect(() => {
    onLoad();
  }, []);

  return (
    <>
      <h1>Guestbook</h1>
      <p>See what people wrote about us and feel free to leave a message.</p>
      {!loading ? (
        messages.map((x, i) => <MessageComponent data={x} key={i} />)
      ) : (
        <p>loading...</p>
      )}
      <Link to="/messages">leave a message</Link>
    </>
  );
};

export default App;
