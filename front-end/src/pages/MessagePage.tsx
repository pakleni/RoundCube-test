import React, { useState } from 'react';
import { newMessage } from '../api/messagesApi';

type RequestStatus = 'sending' | 'success' | 'failed';

const MessageProps: React.FC = () => {
  const [name, setName] = useState('');
  const [message, setMessage] = useState('');
  const [status, setStatus] = useState<RequestStatus>();

  const onSubmit: React.FormEventHandler<HTMLFormElement> = async (event) => {
    setStatus('sending');
    try {
      newMessage({ name, message });
      setStatus('success');
    } catch (e) {
      setStatus('failed');
    }
    event.preventDefault();
  };

  return (
    <form onSubmit={onSubmit}>
      <label htmlFor="name">Name</label>
      <br />
      <input
        type="text"
        id="name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <br />
      <label htmlFor="message">Message</label>
      <br />
      <input
        type="text"
        id="message"
        value={message}
        onChange={(e) => setMessage(e.target.value)}
      />
      <br />
      <input type="submit" value="Post" disabled={!!status} />
      {!!status && <p>{status}</p>}
    </form>
  );
};

export default MessageProps;
