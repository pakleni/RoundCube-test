import express from "express";
import Message from "../models/message";

export class MessageController {
  getLast10 = async (req: express.Request, res: express.Response) => {
    try {
      const messages = await Message.find().sort({ $natural: -1 }).limit(10);

      res.status(200).json(messages);
    } catch (e) {
      console.log("[server] " + e);
    }
  };

  addMessage = async (req: express.Request, res: express.Response) => {
    const { name, message } = req.body;

    try {
      await new Message({ name, message }).save();
      res.status(200).json({ message: "success" });
    } catch (e) {
      res.status(400).json({ message: "error" });
    }
  };
}
