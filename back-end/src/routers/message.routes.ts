import express from "express";
import { MessageController } from "../controllers/message.controller";

const messageRouter = express.Router();

messageRouter
  .route("/")
  .get((req, res) => new MessageController().getLast10(req, res));

messageRouter
  .route("/")
  .post((req, res) => new MessageController().addMessage(req, res));

export default messageRouter;
