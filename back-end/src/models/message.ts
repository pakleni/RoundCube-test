import mongoose from "mongoose";

const Schema = mongoose.Schema;

interface IMessage {
  name: string;
  message: string;
}

const messageSchema = new Schema<IMessage>({
  name: { type: String, required: true },
  message: { type: String, required: true },
});

export default mongoose.model<IMessage>("Message", messageSchema, "messages");
